using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Shapes;

namespace _2048attempt
{
    class block
    {
        public bool grow;
        public string color;
        public int value;
        public int gridSize;
        public int x;
        public int y;
        public int z;
        public Rectangle rbox;
        public TextBlock textbox;
        public block(MainPage mp, int x, int y, int g)
        {
            value = 0;
            color = "#FFC8C8C8";
            gridSize = g;
            this.x = x;
            this.y = y;
            z = 1 + x + y * g; //1D array value
            switch (z) //temp until i learn how to make a grid array of boxes in UWP
            {
                case 1:
                    rbox = mp.box1;
                    textbox = mp.textBlock1;
                    break;
                case 2:
                    rbox = mp.box2;
                    textbox = mp.textBlock2;
                    break;
                case 3:
                    rbox = mp.box3;
                    textbox = mp.textBlock3;
                    break;
                case 4:
                    rbox = mp.box4;
                    textbox = mp.textBlock4;
                    break;
                case 5:
                    rbox = mp.box5;
                    textbox = mp.textBlock5;
                    break;
                case 6:
                    rbox = mp.box6;
                    textbox = mp.textBlock6;
                    break;
                case 7:
                    rbox = mp.box7;
                    textbox = mp.textBlock7;
                    break;
                case 8:
                    rbox = mp.box8;
                    textbox = mp.textBlock8;
                    break;
                case 9:
                    rbox = mp.box9;
                    textbox = mp.textBlock9;
                    break;
                case 10:
                    rbox = mp.box10;
                    textbox = mp.textBlock10;
                    break;
                case 11:
                    rbox = mp.box11;
                    textbox = mp.textBlock11;
                    break;
                case 12:
                    rbox = mp.box12;
                    textbox = mp.textBlock12;
                    break;
                case 13:
                    rbox = mp.box13;
                    textbox = mp.textBlock13;
                    break;
                case 14:
                    rbox = mp.box14;
                    textbox = mp.textBlock14;
                    break;
                case 15:
                    rbox = mp.box15;
                    textbox = mp.textBlock15;
                    break;
                case 16:
                    rbox = mp.box16;
                    textbox = mp.textBlock16;
                    break;
                default:
                    break;

            }
        }
        public void Set (int v)
        {
            value = v;
            switch (value)
            {
                case 2:
                    color = "#FFFFC500";
                    break;
                case 4:
                    color = "#FFFF9700";
                    break;
                case 8:
                    color = "#FFFD6800";
                    break;
                case 16:
                    color = "#FFFF4600";
                    break;
                case 32:
                    color = "#FFFF0C00";
                    break;
                case 64:
                    color = "#FFFF0046";
                    break;
                case 128:
                    color = "#FFFF0097";
                    break;
                case 256:
                    color = "#FFAE00FF";
                    break;
                case 512:
                    color = "#FF5D00FF";
                    break;
                case 1024:
                    color = "#FF0C00FF";
                    break;
                case 2048:
                    color = "#FF0068FF";
                    break;
                case 4096:
                    color = "#FF00AEFF";
                    break;
                case 8144:
                    color = "#FF00FFD1";
                    break;
                case 16288:
                    color = "#FF00FF80";
                    break;
                case 32576:
                    color = "#FF00FF17";
                    break;
                case 65152:
                    color = "#FF000000";
                    break;
                default:
                    color = "#FFC8C8C8";
                    break;

            }
        }
        public string GetText()
        {
            if (value == 0) return "";
            return value.ToString();
        }
        public void SetGrowAnim(bool b)
        {
            if (b) grow = true;
            else grow = false;
        }
    }
}