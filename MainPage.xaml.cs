using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Windows.ApplicationModel.Contacts;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _2048attempt
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        block[,] array2048 = new block[4, 4];
        List<Storyboard> sbQueue = new List<Storyboard>();
        PointerPoint point;
        StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
        StorageFile saveFile;
        bool horizontalTest;
        bool verticalTest;
        bool leftEnabled;
        bool rightEnabled;
        bool upEnabled;
        bool downEnabled;
        int gridSize = 4;
        int score;
        int highscore;
        bool win;
        bool loaded;
        string savetext;
        public static MainPage mainPage;
        public MainPage()
        {
            this.InitializeComponent();
            mainPage = this;
            boxesbutton.AddHandler(PointerPressedEvent, new PointerEventHandler(boxesbutton_PointerPressed), true);
            LoadSave();
            StartGame();

        }
        private async void LoadSave() //load save file
        {
            saveFile = await storageFolder.CreateFileAsync("save.txt", CreationCollisionOption.OpenIfExists);
            savetext = await FileIO.ReadTextAsync(saveFile);
            try
            {
                int result = Int32.Parse(savetext);
                highscore = result;
            }
            catch (FormatException)
            {
                highscore = 0;
            }
            loaded = true;
        }
        private async void RewriteSave()
        {
            await FileIO.WriteTextAsync(saveFile, highscore.ToString());
        }

        public void StartGame()
        {
            gameoverbox.Text = "";
            win = false;
            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    array2048[x, y] = new block(mainPage, x, y, gridSize);
                }
            }
            PlaceRandomly();
            Task.Delay(1).Wait();
            EndTurn();
        }
        public void EndTurn()
        {
            PlaceRandomly();
            UpdateField();
            AnimationHandler();
            GameOverTest();
        }

        private void boxesbutton_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            point = e.GetCurrentPoint(boxes);
            UpArrow.Opacity = 0;
            DownArrow.Opacity = 0;
            LeftArrow.Opacity = 0;
            RightArrow.Opacity = 0;
            if (point.Position.X >= 720 && point.Position.Y < 581 && point.Position.Y > 165 && rightEnabled) //right bound
            {
                ArrowAnim("RightArrow");
                MoveRight();
            }
            else if (point.Position.X <= 300 && point.Position.Y < 581 && point.Position.Y > 165 && leftEnabled) //left bound
            {
                ArrowAnim("LeftArrow");
                MoveLeft();
            }
            else if (point.Position.Y >= 581 && point.Position.X < 720 && point.Position.X > 300 && downEnabled) //bottom bound
            {
                ArrowAnim("DownArrow");
                MoveDown();
            }
            else if (point.Position.Y <= 165 && point.Position.X < 720 && point.Position.X > 300 && upEnabled) //top bound
            {
                ArrowAnim("UpArrow");
                MoveUp();
            }
        }
        private void ArrowAnim (string a)
        {
            ArrowStoryboard.SetValue(Storyboard.TargetNameProperty, a);
            ArrowStoryboard.Begin();
        }
        private Storyboard GrowAnim (block b)
        {
            Storyboard sb = new Storyboard();
            DoubleAnimation dAnimX = new DoubleAnimation
            {
                From = 0.5,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 250))
                
            };
            DoubleAnimation dAnimY = new DoubleAnimation
            {
                From = 0.5,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 250))

            };
            sb.Children.Add(dAnimX);
            sb.Children.Add(dAnimY);
            Storyboard.SetTarget(dAnimX, b.rbox);
            Storyboard.SetTargetProperty(dAnimX, "(UIElement.RenderTransform).(ScaleTransform.ScaleX)");
            Storyboard.SetTarget(dAnimY, b.rbox);
            Storyboard.SetTargetProperty(dAnimY, "(UIElement.RenderTransform).(ScaleTransform.ScaleY)");
            return sb;
        }
        private Storyboard ShrinkAnim (block b)
        {
            Storyboard sb = new Storyboard();
            DoubleAnimation dAnimX = new DoubleAnimation
            {
                From = 1.1,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 250))

            };
            DoubleAnimation dAnimY = new DoubleAnimation
            {
                From = 1.1,
                To = 1,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 250))

            };
            sb.Children.Add(dAnimX);
            sb.Children.Add(dAnimY);
            Storyboard.SetTarget(dAnimX, b.rbox);
            Storyboard.SetTargetProperty(dAnimX, "(UIElement.RenderTransform).(ScaleTransform.ScaleX)");
            Storyboard.SetTarget(dAnimY, b.rbox);
            Storyboard.SetTargetProperty(dAnimY, "(UIElement.RenderTransform).(ScaleTransform.ScaleY)");
            return sb;
        }
        private void AnimationHandler ()
        {
            foreach (Storyboard sb in sbQueue) sb.Begin();
            sbQueue.Clear();
        }
        private void boxesbutton_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            ArrowStoryboard.Stop();
            if (upEnabled) UpArrow.Opacity = 100;
            if (downEnabled) DownArrow.Opacity = 100;
            if (leftEnabled) LeftArrow.Opacity = 100;
            if (rightEnabled) RightArrow.Opacity = 100;
        }

        public void PlaceRandomly()
        {
            Random rnd = new Random();
            int flip = rnd.Next(2);
            int value;
            if (flip == 1) value = 4;
            else value = 2;
            bool test = false;
            do
            {
                int x = rnd.Next(array2048.GetLength(0));
                int y = rnd.Next(array2048.GetLength(1));
                if (array2048[x, y].value == 0)
                {
                    array2048[x, y].Set(value);
                    test = true;
                    array2048[x, y].SetGrowAnim(true);
                    sbQueue.Add(GrowAnim(array2048[x, y]));

                }
            }
            while (!test);
        }
        public void GameOverTest()
        {
            if (loaded && score > highscore)
            {
                highscore = score;
                highscorebox.Text = score.ToString();
                RewriteSave();
            }
            else if (loaded) highscorebox.Text = highscore.ToString();
            leftEnabled = LeftTest();
            rightEnabled = RightTest();
            upEnabled = UpTest();
            downEnabled = DownTest();
            if (!leftEnabled && !rightEnabled && !downEnabled && !upEnabled && win)
            {
                gameoverbox.Text = "You won! Play Again?";
            }
            else if (!leftEnabled && !rightEnabled && !downEnabled && !upEnabled)
            {
                gameoverbox.Text = "Game Over, Try Again?";
                boxesbutton.Opacity = 100;
            }
            else if (!win) CheckFor2048();
        }

        public void CheckFor2048()
        {
            foreach (block b in array2048)
            {
                if (b.value >= 2048)
                {
                    gameoverbox.Text = "You win! Keep Going!";
                    win = true;
                    break;
                }
            }
        }

        public bool LeftTest() //tests if you can move left, must be done BEFORE RightTest
        {
            horizontalTest = false;
            bool lt = false;
            for (int y = 0; y < array2048.GetLength(1); y++)
            {
                for (int x = 0; x < array2048.GetLength(0) - 1; x++)
                {
                    if (array2048[x, y].value == array2048[x + 1, y].value && array2048[x, y].value != 0)
                    {
                        horizontalTest = true;
                        return true;
                    }
                    else if (array2048[x, y].value == 0 && array2048[x + 1, y].value != 0) lt = true;
                }
            }
            if (lt) return true;
            return false;
        }
        public bool RightTest() //tests if you can move right, must be done AFTER LeftTest
        {
            if (horizontalTest) return true;
            for (int y = 0; y < array2048.GetLength(1); y++)
            {
                for (int x = array2048.GetLength(0) - 1; x > 0; x--)
                {
                    if (array2048[x, y].value == 0 && array2048[x - 1, y].value != 0) return true;
                }
            }
            return false;
        }

        public bool UpTest() //tests if you can move up, must be done BEFORE DownTest
        {
            verticalTest = false;
            bool ut = false;
            for (int x = 0; x < array2048.GetLength(0); x++)
            {
                for (int y = 0; y < array2048.GetLength(1) - 1; y++)
                {
                    if (array2048[x, y].value == array2048[x, y + 1].value && array2048[x, y].value != 0)
                    {
                        verticalTest = true;
                        return true;
                    }
                    else if (array2048[x, y].value == 0 && array2048[x, y + 1].value != 0) ut = true;
                }
            }
            if (ut) return true;
            return false;
        }

        public bool DownTest() //tests if you can move down, must be done AFTER UpTest
        {
            if (verticalTest) return true;
            for (int x = 0; x < array2048.GetLength(0); x++)
            {
                for (int y = array2048.GetLength(1) - 1; y > 0; y--)
                {
                    if (array2048[x, y].value == 0 && array2048[x, y - 1].value != 0) return true;
                }
            }
            return false;
        }

        public void ShiftLeft(int y) //moves populated tiles to the left
        {
            for (int x = 0; x < array2048.GetLength(0) - 1; x++)
            {
                if (array2048[x, y].value == 0)
                {
                    for (int z = x + 1; z < array2048.GetLength(0); z++)
                    {
                        if (array2048[z, y].value != 0)
                        {
                            array2048[x, y].Set(array2048[z, y].value);
                            array2048[z, y].Set(0);
                            break;
                        }
                    }
                }

            }
        }
        public void MoveLeft()
        {
            for (int y = 0; y < array2048.GetLength(1); y++)
            {
                ShiftLeft(y);
                for (int x = 0; x < array2048.GetLength(0) - 1; x++) //combines adjacent tiles starting left
                {
                    if (array2048[x, y].value == array2048[x + 1, y].value && array2048[x, y].value != 0)
                    {
                        array2048[x, y].Set(array2048[x, y].value * 2);
                        sbQueue.Add(ShrinkAnim(array2048[x, y]));
                        array2048[x + 1, y].Set(0);
                        ShiftLeft(y);
                    }
                }
            }
            EndTurn();
        }

        public void ShiftRight(int y) //moves populated tiles to the right
        {
            for (int x = array2048.GetLength(0) - 1; x > 0; x--)
            {
                if (array2048[x, y].value == 0)
                {
                    for (int z = x - 1; z >= 0; z--)
                    {
                        if (array2048[z, y].value != 0)
                        {
                            array2048[x, y].Set(array2048[z, y].value);
                            array2048[z, y].Set(0);
                            break;
                        }
                    }
                }
            }
        }
        public void MoveRight()
        {
            for (int y = 0; y < array2048.GetLength(1); y++)
            {
                ShiftRight(y);
                for (int x = array2048.GetLength(0) - 1; x > 0; x--) //combines adjacent tiles starting right
                {
                    if (array2048[x, y].value == array2048[x - 1, y].value && array2048[x, y].value != 0)
                    {
                        array2048[x, y].Set(array2048[x, y].value * 2);
                        sbQueue.Add(ShrinkAnim(array2048[x, y]));
                        array2048[x - 1, y].Set(0);
                        ShiftRight(y);
                    }
                }
            }
            EndTurn();
        }

        public void ShiftUp(int x) //moves populated tiles to the top
        {
            for (int y = 0; y < array2048.GetLength(1) - 1; y++)
            {
                if (array2048[x, y].value == 0)
                {
                    for (int z = y + 1; z < array2048.GetLength(1); z++)
                    {
                        if (array2048[x, z].value != 0)
                        {
                            array2048[x, y].Set(array2048[x, z].value);
                            array2048[x, z].Set(0);
                            break;
                        }
                    }
                }
            }
        }

        public void MoveUp()
        {
            for (int x = 0; x < array2048.GetLength(0); x++)
            {
                ShiftUp(x);
                for (int y = 0; y < array2048.GetLength(1) - 1; y++) //combines adjacent tiles starting up
                {
                    if (array2048[x, y].value == array2048[x, y + 1].value && array2048[x, y].value != 0)
                    {
                        array2048[x, y].Set(array2048[x, y].value * 2);
                        sbQueue.Add(ShrinkAnim(array2048[x, y]));
                        array2048[x, y + 1].Set(0);
                        ShiftUp(x);
                    }
                }
            }
            EndTurn();
        }

        public void ShiftDown(int x)
        {
            for (int y = array2048.GetLength(1) - 1; y > 0; y--) //moves populated tiles to the right
            {
                if (array2048[x, y].value == 0)
                {
                    for (int z = y - 1; z >= 0; z--)
                    {
                        if (array2048[x, z].value != 0)
                        {
                            array2048[x, y].Set(array2048[x, z].value);
                            array2048[x, z].Set(0);
                            break;
                        }
                    }
                }
            }
        }
        public void MoveDown()
        {
            for (int x = 0; x < array2048.GetLength(0); x++)
            {
                ShiftDown(x);
                for (int y = array2048.GetLength(1) - 1; y > 0; y--) //combines adjacent tiles starting right
                {
                    if (array2048[x, y].value == array2048[x, y - 1].value && array2048[x, y].value != 0)
                    {
                        array2048[x, y].Set(array2048[x, y].value * 2);
                        sbQueue.Add(ShrinkAnim(array2048[x, y]));
                        array2048[x, y - 1].Set(0);
                        ShiftDown(x);
                    }
                }
            }
            EndTurn();
        }


        public SolidColorBrush GetSolidColorBrush(string hex) //this method is because there's no decent references in c# that converts hex color codes for some reason
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            SolidColorBrush myBrush = new SolidColorBrush(Windows.UI.Color.FromArgb(a, r, g, b));
            return myBrush;
        }

        public void UpdateField() //updates grid colors and text, as well as score
        {
            score = 0;
            foreach (block b in array2048)
            {
                b.rbox.Fill = GetSolidColorBrush(b.color);
                b.textbox.Text = b.GetText();
                score += b.value;
            }
            scorebox.Text = score.ToString();
            Canvas.SetZIndex(boxesbutton, 1);
        }

        private void newgamebutton_Click(object sender, RoutedEventArgs e)
        {
            boxesbutton.Opacity = 0;
            StartGame();
        }
    }
}
